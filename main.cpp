﻿
/*Programa que crea una figura con forma de rombo usando asteriscos (*), el numero de asteriscos
 * a lo alto y a lo ancho esta determinado por el numero ingresado por teclado.
 * Este programa requiere que se ingrese un numero impar en caso de ingresar un numero par
 * El programa le pedira que intente de nuevo hasta que ingrese un numero impar y le mostrara la figura.
 *El programa acepta los numeros impares y rechaza los pares sin importar si son positivos o negativos.
*/
#include <iostream>
using namespace std;

int main()
{
    int numero,fila,col;
cout<<"ingrese un mnumero impar: ";
cin>>numero;
    if(numero<0)                              //cambiando numero negativo apositivo
        numero=numero*-1;

while (numero%2==0){
    cout<<"ingreso un numeor impar intente de nuevo: ";cin>>numero;}
    if(numero<0)                            //cambiando numero negativo apositivo
        numero=numero*-1;
    else{

    for(fila=1;fila<=numero;fila=fila+2) {  //este ciclo define las filas de la columna  para el lado superior del rombo
        for(col=1;col<=numero-fila;col=col+2) //escribir espacios disminuidos en 2 cada que columna aumenta en 2
           cout<<(" ");
        for(col=1;col<=fila;col++)//escribir asterisco minetras que columna sea menor o igual que fila, sin espacios entre los asteriscos
            cout<<("*");
            cout<<endl;         //saltar renglon cada que escribe los asteriscos


     }
     for(fila=numero-2;fila>=0;fila=fila-2) {//parte inferior del rombo inicia cuando se ha escrito un numero de asteriscos igual al numero ingresado
        for(col=1;col<=numero-fila;col=col+2) //la parte inferior tiene una fila menos que la superior
           cout<<(" ");
        for(col=1;col<=fila;col++)
            cout<<("*");
            cout<<endl;

    }
    }
    return 0;
}
